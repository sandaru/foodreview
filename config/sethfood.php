<?php


return [
    // SITE CONFIG
    'SITE_NAME'                             => "B2BE Food Review",

    // MAX COMMENT
    'MAX_COMMENT_PER_DAY'                   => 10,

    // SEND EMAIL FOR NEW VOTE ?
    'SEND_EMAIL_NEW_VOTE'                   => env('VOTE_EMAIL_NOTIFICATION'),
    'SYSTEM_EMAIL'                          => 'no-reply@sethphat.com', // sender system email
    'SYSTEM_EMAIL_NAME'                     => 'SethPhat System',

    // RESULT NUMBER
    'LIMIT_ITEM'                            => 12,

    // DATE TIME FORMAT
    'DATETIME_SHOW_FORMAT'                  => 'd/m/Y H:i:s',
    'DATE_SHOW_FORMAT'                      => 'd/m/Y',

    // UPLOAD PATH
    'UPLOAD_POST_PATH'                      => '/uploads/post_attachment',
    'UPLOAD_ATTACHMENT_PATH'                => '/uploads/comment_attachment',
    'UPLOAD_AVATAR_PATH'                    => '/uploads/user_attachment',
    'UPLOAD_GROUP_PATH'                     => '/uploads/group_attachment',
];