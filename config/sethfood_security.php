<?php


return [
    // list api need to check user id + token
    'AUTHENTICATION_API' => [
        'Comment@PostComment',

        'Post@CreatePost',
        'Post@GetOptions',

        'User@UpdateProfile',
        'User@UpdatePassword',
        'User@GetMembers',

        // group information
        'Group@GetUserGroup',
        'Group@CreateGroup',
        'Group@GetByID',

        // vote info
        'VotePlace@CreateVote',
        'VotePlace@EditVote',
        'VotePlace@DeleteVote',
        'VotePlaceItemEntry@Vote',
    ],
];