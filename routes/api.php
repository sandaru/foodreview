<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * Dynamic route by Seth Phat
 * Usage: domain/v1/<controller_name>/<method_name>/<id_if_need>
 * Note: if you use id, receive it in your method function as $id variable only.
 */
Route::middleware(['cors', 'api_token'])->prefix('v1')->group(function () {
    Route::any('/{controller}/{function}/{id?}', function ($controller, $name, $id = null) {
        if(!file_exists(app_path('Http/Controllers/Api/'.$controller.'Controller.php'))) {
            return json_encode(['error' => 'Controller does not existed.']);
        }

        try {
            return App::call("App\\Http\\Controllers\\Api\\" . $controller . "Controller@$name", [$id]);
        } catch (\Exception $e) {
            \Illuminate\Support\Facades\Log::error("[ERROR when access] $controller@$name (ID?: $id) => {$e->getMessage()}");
            return response()->json(['error' => 'Method does not existed or missing parameter.']);
        }
    });
});
