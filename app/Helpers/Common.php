<?php

function encode_id($str) {
    return \Illuminate\Support\Facades\Crypt::encryptString($str);
}

function decode_id($str) {
    try {
        $decode = \Illuminate\Support\Facades\Crypt::decryptString($str);
        return $decode;
    } catch (\Exception $e) {
        return null;
    }
}


/**
 * Encrypt for password only
 * @param $str
 * @return string
 */
function password_encrypt($str)
{
    $arrStr = str_split($str);

    $pass = "";
    foreach ($arrStr as $char)
        $pass .= sha1($char);

    return sha1(md5($pass . sha1($pass)));
}

/**
 * Set logged data for SQL
 * @param $obj
 * @param $userObj
 * @param bool $created
 */
function setObjLoggedInfo(&$obj, $userObj, $created = false)
{
    if ($created) {
        $obj->CreatedDate = now();
        $obj->CreatedByName = $userObj->FullName;
        $obj->CreatedByID = $userObj->ID;
    }

    $obj->ModifiedDate = now();
    $obj->ModifiedByName = $userObj->FullName;
    $obj->ModifiedByID = $userObj->ID;
}

function generate_unique_slug($table, $column, $value) {
    $now = 0;
    $old = $value;
    do {
        $isExisted = \Illuminate\Support\Facades\DB::table($table)
                                    ->where($column, $value)
                                    ->count();

        if (!$isExisted) {
            return $value;
        }
        $value = $old . "-" . (++$now);
    } while (true);
}