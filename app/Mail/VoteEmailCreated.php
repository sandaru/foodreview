<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class VoteEmailCreated extends Mailable
{
    use Queueable, SerializesModels;

    private $groupInfo;
    private $voteInfo;
    private $user_name;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($groupInfo, $voteInfo, $user_name)
    {
        $this->groupInfo = $groupInfo;
        $this->voteInfo = $voteInfo;
        $this->user_name = $user_name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // set some info...
        $this->subject("Thông báo về bình chọn địa điểm vừa được khởi tạo");
        $this->from(config("sethfood.SYSTEM_EMAIL"), config("sethfood.SYSTEM_EMAIL_NAME"));

        return $this->view('email.vote_email', [
            'groupInfo' => $this->groupInfo,
            'voteInfo' => $this->voteInfo,
            'user_name' => $this->user_name,
        ]);
    }
}
