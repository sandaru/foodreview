<?php

namespace App\Http\Middleware;

use App\Models\UserModel;
use Closure;
use Illuminate\Support\Facades\Session;

class LoggedAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user_id = Session::get("USER_ADMIN_ID", null);
        $user_pass = Session::get("USER_ADMIN_PASS", null);

        if(empty($user_id) || empty($user_pass)) {
            Session::remove("USER_ADMIN_ID");
            Session::remove("USER_ADMIN_PASS");
            return redirect()->route('loginPage');
        }

        return $next($request);
    }
}
