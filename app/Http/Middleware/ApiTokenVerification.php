<?php

namespace App\Http\Middleware;

use App\Models\UserModel;
use Closure;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;

class ApiTokenVerification
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $list_api_strict = config('sethfood_security.AUTHENTICATION_API');
        $api_name = $request->route('controller') . "@" . $request->route('function');

        // if this api need to check
        if (in_array($api_name, $list_api_strict)) {
            Log::debug("[$api_name] Start to check permission");
            // check header first
            $header = getallheaders();

            if (!isset($header['Seth_Token']) || !isset($header['Seth_UserID'])) {
                Log::debug("[$api_name] Failed due to missing headers");
                return response()->json(['error' => 'Not authorized.'], 401);
            }

            // ok check user id and token
            $user_id = decode_id($header['Seth_UserID']);
            $token = $header['Seth_Token'];

            // find user
            $user = UserModel::FindTokenAndUserID($user_id, $token);
            if ($user == null) {
                Log::debug("[$api_name] Failed due to user not found");
                return response()->json(['error' => 'Not authorized.'], 401);
            }
        }

        return $next($request);
    }
}
