<?php

namespace App\Http\Controllers\Api;

use App\Models\GroupMemberModel;
use App\Models\VotePlaceItemEntryModel;
use App\Models\VotePlaceItemModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;

class VotePlaceItemEntryController extends Controller
{
    //
    public function Vote($placeItemID) {
        $votePlace = VotePlaceItemModel::find(decode_id($placeItemID));
        if ($votePlace == null) {
            return response()->json(['error' => __('api.vote_item_not_existed')]);
        }

        $vote = $votePlace->Vote;
        $user = $this->current_user();

        // check if user in group or not
        if(!GroupMemberModel::IsUserInGroup($vote->GroupID, $user->ID)) {
            // not in group
            return response()->json(['error' => __('api.not_in_group')]);
        }

        // check if this vote this have time to vote
        $time = Carbon::parse($vote->EndTime);
        $now = Carbon::createFromTime(date('H'), date('i'), date('s'));
        $created_date = Carbon::parse($vote->CreatedDate);

        if (!($time->gt($now) && $created_date->isToday())) {
            return response()->json(['error' => __('api.vote_out_of_time')]);
        }

        // start to vote
        if (VotePlaceItemEntryModel::IsVoted($user->ID, $votePlace->ID)) {
            // remove vote
            VotePlaceItemEntryModel::RemoveVote($user->ID, $votePlace->ID);
        } else {
            // not vote yet, vote this
            VotePlaceItemEntryModel::create([
                'UserID' => $user->ID,
                'VotePlaceItemID' => $votePlace->ID,
                'CreatedDate' => now()
            ]);
        }

        return response()->json(['success' => 'success']);
    }

    public function ViewEntry($placeItemID) {
        $votePlace = VotePlaceItemModel::find(decode_id($placeItemID));
        if ($votePlace == null) {
            return response()->json(['error' => __('api.vote_item_not_existed')]);
        }

        $result_data = [];
        $entries = $votePlace->Entries()->get();
        foreach($entries as $entry) {
            $result_data[] = $entry->CompiledData;
        }

        return response()->json($result_data);
    }
}
