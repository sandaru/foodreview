<?php
/**
 * Created by PhpStorm.
 * User: Seth Phat
 * Date: 8/18/2018
 * Time: 2:05 PM
 */

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Jobs\SendEmailForVote;
use App\Models\GroupMemberModel;
use App\Models\GroupModel;
use App\Models\PostModel;
use App\Models\VotePlaceItemEntryModel;
use App\Models\VotePlaceItemModel;
use App\Models\VotePlaceModel;
use App\QueryModels\ModelManager;
use Illuminate\Support\Carbon;

class VotePlaceController extends Controller
{
    public function GetByID($id) {
        $vote = VotePlaceModel::find(decode_id($id));
        if ($vote === null) {
            return response()->json(['error' => __('api.vote_not_existed')]);
        }

        $data = $vote->CompiledData;
        $data['IsInGroup'] = false;
        $data['CanEdit'] = false;
        $data['CanVote'] = false;

        // check if user in this group
        $user = $this->current_user();
        if ($user != null) {
            // check if this user in group or not
            $data['IsInGroup'] = GroupMemberModel::IsUserInGroup($vote->GroupID, $user->ID);

            // check if the one create this or not or admin of this group
            if ($user->ID === $vote->CreatorUserID || $user->ID === $vote->Group->AdminUserID) {
                $data['CanEdit'] = true;
            }

            // check time if can vote
            $time = Carbon::parse($vote->EndTime);
            $now = Carbon::createFromTime(date('H'), date('i'), date('s'));
            $created_date = Carbon::parse($vote->CreatedDate);

            if ($time->gt($now) && $created_date->isToday()) {
                $data['CanVote'] = true;
            }
        }

        // if user in group, get data to check if this user voted or not
        if ($data['IsInGroup']) {
            foreach ($data['Items'] as $index => $vote_item) {
                $data['Items'][$index]['Vote'] = VotePlaceItemEntryModel::IsVoted($user->ID, decode_id($vote_item['ID']));
            }
        }

        return response()->json($data);
    }

    public function CreateVote($id) {
        // find group if existed
        $group = GroupModel::find(decode_id($id));
        if ($group == null) {
            return response()->json(['error' => __('api.group_not_existed')]);
        }

        // validate information
        $rules = [
            'Title' => 'required|max:30',
            'EndTime' => 'required|date_format:H:i|after:StartTime',
            'Places' => 'required|array|min:2',
            'Places.*' => 'required|min:1|distinct'
        ];

        $dataValid = $this->runValidator($rules, 'POST', ['StartTime' => now()->format('H:i')]);
        if (!$dataValid['Valid']) {
            return $this->return_validator_error($dataValid);
        }

        // prepare
        $current_user = $this->current_user();

        // check if user in group or not
        if(!GroupMemberModel::IsUserInGroup($group->ID, $current_user->ID)) {
            // not in group
            return response()->json(['error' => __('api.not_in_group')]);
        }

        // create new vote
        $vote = new VotePlaceModel;
        $vote->Title = $dataValid['Data']['Title'];
        $vote->EndTime = $dataValid['Data']['EndTime'];
        $vote->GroupID = $group->ID;
        $vote->CreatorUserID = $current_user->ID;
        setObjLoggedInfo($vote, $current_user, true);
        $vote->save();

        // loop all slug places...
        foreach ($dataValid['Data']['Places'] as $post_slug) {
            $postItem = PostModel::bySlug($post_slug)
                                    ->byActivated()
                                    ->byVerified()
                                    ->first();
            if ($postItem == null) {
                continue;
            }

            // insert now
            VotePlaceItemModel::create([
                'PostID' => $postItem->ID,
                'VotePlaceID' => $vote->ID
            ]);
        }

        // send email
        SendEmailForVote::dispatch($group->ID, $vote->ID);

        // done
        return response()->json(['success' => __('api.created_vote')]);
    }

    public function EditVote($vote_id) {
        // find group if existed
        $vote = VotePlaceModel::find(decode_id($vote_id));
        if ($vote == null) {
            return response()->json(['error' => __('api.vote_not_existed')]);
        }

        $user = $this->current_user();

        // check if user in group or not
        if(!GroupMemberModel::IsUserInGroup($vote->GroupID, $user->ID)) {
            // not in group
            return response()->json(['error' => __('api.not_in_group')]);
        }

        // check if user not the creator of this vote or not group admin
        if ($vote->CreatorUserID !== $user->ID && $vote->Group->AdminUserID !== $user->ID) {
            return response()->json(['error' => __('api.permission_denied')]);
        }

        // check if this vote this have time to vote
        $time = Carbon::parse($vote->EndTime);
        $now = Carbon::createFromTime(date('H'), date('i'), date('s'));
        $created_date = Carbon::parse($vote->CreatedDate);
        if (!($time->gt($now) && $created_date->isToday())) {
            // out of time
            return response()->json(['error' => __('api.vote_out_of_time_edit')]);
        }

        // validate information
        $rules = [
            'Places' => 'required|array|min:2',
            'Places.*' => 'required|min:1|distinct'
        ];

        $dataValid = $this->runValidator($rules, 'POST');
        if (!$dataValid['Valid']) {
            return $this->return_validator_error($dataValid);
        }

        // ok edit info now
        $new_items = $dataValid['Data']['Places'];

        // remove old one
        $votePlaceItemModel = ModelManager::getInstance()->getModel("VotePlaceItem");
        $old_items = $votePlaceItemModel->getCurrentItemSlug($vote->ID);
        foreach ($old_items as $item) {
            if (!in_array($item->Slug, $new_items)) {
                VotePlaceItemModel::find($item->ItemID)->delete();
            }
        }

        // ok new one coming up
        foreach ($new_items as $post_slug) {
            $postItem = PostModel::bySlug($post_slug)
                            ->byActivated()
                            ->byVerified()
                            ->first();
            if ($postItem == null) {
                continue;
            }

            // if this item already existed
            $find_item = $old_items->where('PostID', $postItem->ID);
            if ($find_item->count() <= 0) {
                // insert new
                VotePlaceItemModel::create([
                    'PostID' => $postItem->ID,
                    'VotePlaceID' => $vote->ID
                ]);
            }
        }


        return response()->json(['success' => __('api.edited_vote')]);
    }

    public function DeleteVote($id) {
        // find group if existed
        $vote = VotePlaceModel::find(decode_id($id));
        if ($vote == null) {
            return response()->json(['error' => __('api.vote_not_existed')]);
        }

        $user = $this->current_user();

        // check if user in group or not
        if(!GroupMemberModel::IsUserInGroup($vote->GroupID, $user->ID)) {
            // not in group
            return response()->json(['error' => __('api.not_in_group')]);
        }

        // check if user not the creator of this vote or not group admin
        if ($vote->CreatorUserID !== $user->ID && $vote->Group->AdminUserID !== $user->ID) {
            return response()->json(['error' => __('api.permission_denied')]);
        }

        // ok delete
        $vote->delete();
        return response()->json(['success' => __('api.deleted_vote')]);
    }
}