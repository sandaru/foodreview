<?php

namespace App\Http\Controllers\Api;

use App\Models\CategoryModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function GetBySlug($slug) {
        $cate = CategoryModel::bySlug($slug)->byActivate()->first();
        if ($cate == null) {
            return response()->json(['error' => __('api.category_not_existed')]);
        }

        return response()->json($cate->CompiledData);
    }
}
