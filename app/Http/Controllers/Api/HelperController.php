<?php

namespace App\Http\Controllers\Api;

use App\Models\PostModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HelperController extends Controller
{
    CONST SUPPORTED_CLASS = ['Post', 'Category'];

    //
    public function GenerateSlug($id) {
        if (empty($id) || !in_array($id, self::SUPPORTED_CLASS)) {
            return "RUNNING FAILED DUE TO WRONG KEY!";
        }

        $class_name = "App\\Models\\" . $id . "Model";
        $classObj = new $class_name;
        $all_objs = $classObj->all();

        $finished = 0;
        foreach ($all_objs as $obj) {
            if (isset($obj->Name)) {
                $obj->Slug = str_slug($obj->Name);
            } else {
                $obj->Slug = str_slug($obj->Title);
            }
            $obj->save();
            $finished++;
        }

        return "SUCCESSFULLY CHANGED FOR $finished ITEM(S)";
    }

    public function ShowAllPost() {
        $all = PostModel::all();

        $html = "";
        foreach($all as $item) {
            $html .= "<p>ID: {$item->ID} - Title: {$item->Title}</p>";
        }

        return $html;
    }

    public function VerifyPost($id) {
        $post = PostModel::find($id);
        if ($post == null) {
            return "POST NOT EXISTS";
        }

        $post->Verified = true;
        $post->save();
        return "APPROVED!";
    }
}
