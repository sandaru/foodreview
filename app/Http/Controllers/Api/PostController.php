<?php

namespace App\Http\Controllers\Api;

use App\Models\AttributeModel;
use App\Models\CategoryModel;
use App\Models\PostAttributeModel;
use App\Models\PostItemModel;
use App\Models\PostModel;
use App\Models\UserModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;

class PostController extends Controller
{
    /**
     * Get By ID
     * @param $id
     */
    public function GetByID($id) {
        $id = decode_id($id);
        $post = PostModel::find($id);
        if ($post == null || $post->Activate == false) {
            return response()->json(['error' => __('api.post_not_existed')]);
        }

        // return
        return response()->json($post->CompiledData);
    }

    public function GetBySlug($id) {
        $post = PostModel::query()->where('Slug', $id)->first();
        if ($post == null || $post->Activate == false || $post->Verified == false) {
            return response()->json(['error' => __('api.post_not_existed')]);
        }

        // return
        return response()->json($post->CompiledData);
    }

    public function GetOptions($type) {
        $posts = PostModel::all()
                        ->where('Activate', true)
                        ->where('Verified', true);

        $result_data = [];
        foreach($posts as $item) {
            $row = [
                'id' => ($type == 'id') ? encode_id($item->ID) : $item->Slug,
                'text' => $item->Title,
                'address' => $item->Address
            ];

            if (!empty($item->GalleryItem)) {
                $row['image'] = $item->GalleryItem[0];
            } else {
                $row['image'] = null;
            }

            $result_data[] = $row;
        }

        return response()->json($result_data);
    }

    public function Search($id = null) {
        $this->load_model("Post_model");
        $keyword = request()->get('keyword', '');
        $start = request()->get('start', 0);
        $limit = request()->get('limit', config('sethfood.LIMIT_ITEM'));

        $result = $this->post_model->SearchPost($keyword, $id, $start, $limit);
        return response()->json($result);
    }

    public function FeaturePost() {
        $this->load_model("Post_model");
        $start = request()->get('start', 0);
        $limit = request()->get('limit', config('sethfood.LIMIT_ITEM'));

        $result = $this->post_model->GetPopularPost($start, $limit);
        return response()->json($result);
    }

    /**
     * Get a new post
     */
    public function CreatePost() {
        $rules = [
            // post
            'CategoryID'     => 'required',
            'Title'           => 'required|min:10|max:50',
            'ShortDescription' => 'max:50',
            'Description' => 'required|min:30',
            'Phone' => 'max:15',
            'Website' => 'max:70',
            'Address' => 'required|max:70',
            'PrepareTime' => 'required|integer|min:1|max:5',
            'MinPrice' => 'required|numeric|min:5000|max:99999999',
            'MaxPrice' => 'required|numeric|min:5000|gt:MinPrice|max:99999999',

            // arr post item
            'PostItems' => 'required|array|min:1',
            'PostItems.*.Title' => 'required|min:5|max:50',
            'PostItems.*.Description' => 'max:100',
            'PostItems.*.Price' => 'required|numeric|min:5000|max:99999999',
            'PostItems.*.Image' => 'sometimes|image|max:3000',

            // arr attributes
            'Attributes' => 'required|array',
        ];

        if (request()->has('Gallery')) {
            $rules['Gallery_FILE'] = 'required|array';
            $rules['Gallery.*'] = 'image|max:3000';
        }

        $dataValid = $this->runValidator($rules);
        if (!$dataValid['Valid']) {
            return $this->return_validator_error($dataValid);
        }

        // check category
        $cate_id = decode_id($dataValid['Data']['CategoryID']);
        $category = CategoryModel::find($cate_id);
        if ($category == null) {
            return response()->json(['error' => __('api.category_not_existed')]);
        }

        // upload path
        $up_path = config('sethfood.UPLOAD_POST_PATH');
        // get current user
        $user = UserModel::find(decode_id(getallheaders()['Seth_UserID']));

        // ok post now LOL
        $post = new PostModel;
        $post->CategoryID = $category->ID;
        $post->Title = strip_tags($dataValid['Data']['Title']);
        $post->ShortDescription = strip_tags($dataValid['Data']['ShortDescription']);
        $post->Description = strip_tags($dataValid['Data']['Description']);
        $post->Phone = strip_tags($dataValid['Data']['Phone']);
        $post->Website = strip_tags($dataValid['Data']['Website']);
        $post->Address = strip_tags($dataValid['Data']['Address']);
        $post->MinPrice = $dataValid['Data']['MinPrice'];
        $post->MaxPrice = $dataValid['Data']['MaxPrice'];
        $post->PrepareTime = __('api.prepare_time_' . $dataValid['Data']['PrepareTime']);
        $post->UniqueSlug = str_slug($post->Title);
        setObjLoggedInfo($post, $user, true);

        // solving upload files
        if (isset($dataValid['Data']['Gallery'])) {
            $img = [];
            foreach($dataValid['Data']['Gallery'] as $file) {
                $uploaded_file_name = $this->process_upload($file, $up_path);
                if ($uploaded_file_name === null) {
                    continue;
                }
                $img[] = $uploaded_file_name;
            }
            $post->Gallery = implode(",", $img);
        }

        if (!$post->save()) {
            return response()->json(['error' => __('api.create_post_failed')]);
        }

        // now we start to insert post item
        foreach($dataValid['Data']['PostItems'] as $index => $item) {
            $post_item = new PostItemModel;
            $post_item->PostID = $post->ID;
            $post_item->Title = $item['Title'];
            $post_item->Description = empty($item['Description']) ? "" : $item['Description'];
            $post_item->Price = $item['Price'];

            // solving upload
            $imgFile = Input::file("PostItems.$index.Image");
            if ($imgFile != null) {
                $post_item->Image = $this->process_upload($imgFile, $up_path, "ITEM");
            }

            // save menu
            setObjLoggedInfo($post_item, $user, true);
            $post_item->save();
        }

        // solving post attribute
        if (isset($dataValid['Data']['Attributes'])) {
            foreach ($dataValid['Data']['Attributes'] as $attr_id) {
                $attr = AttributeModel::find(decode_id($attr_id));
                if ($attr == null) {
                    continue;
                }

                $newPostAttr = new PostAttributeModel;
                $newPostAttr->PostID = $post->ID;
                $newPostAttr->AttributeID = $attr->ID;
                $newPostAttr->save();
            }
        }

        return response()->json(['msg' => __('api.create_post_ok_verify')]);
    }

    private function process_upload($file, $upload_path, $prefix = "POST") {
        if (!$file->isValid()) {
            return null;
        }

        // process add img
        $image = Image::make($file);
        $new_name = $prefix . "_" . time() . "_" . md5($file->getClientOriginalName()) . rand(0,999999) . ".jpg";
        $full_path = $upload_path . "/" . $new_name;

        // process to resize image
        $width = $image->width();
        $height = $image->height();
        if ($file->getSize() > 500) {
            if ($width == $height) {
                // square, so 1000x1000 will be good
                $image->resize('1000', '1000');
            } else if ($width > $height) {
                // horizontal
                $image->resize('1366', '768');
            } else {
                // vertical
                $image->resize('705', '960');
            }
        }

        // save
        $image->save(public_path($full_path), 70);

        return $full_path;
    }
}
