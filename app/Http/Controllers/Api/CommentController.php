<?php

namespace App\Http\Controllers\Api;

use App\Models\CommentModel;
use App\Models\PostModel;
use App\Models\UserLogCommentModel;
use App\Models\UserModel;
use App\QueryModels\Comment_model;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;

/**
 * Class CommentController
 * @package App\Http\Controllers\Api
 * @property Comment_model $comment_model
 */
class CommentController extends Controller
{
    //
    public function GetComment($id) {
        $id = decode_id($id);
        $post = PostModel::find($id);
        if ($post === null || $post->Activate == false) {
            return response()->json(['error' => __('api.post_not_existed')]);
        }

        // get comment
        $start = request()->get('start', 0);
        $limit = request()->get('limit', config('sethfood.LIMIT_ITEM'));

        $result = CommentModel::GetCommentByPostID($post->ID, $start, $limit);
        return response()->json($result);
    }

    public function PostComment($id) {
        $post_id = decode_id($id);
        $post = PostModel::find($post_id);
        if ($post === null) {
            return response()->json(['error' => __('api.post_not_existed')]);
        }

        $rules = [
            'Title' => 'required|min:5|max:50',
            'Comment' => 'required|min:10|max:1500',
            'Rating' => 'required|min:1|max:5',
        ];

        if (request()->has('Gallery')) {
            $rules['Gallery_FILE'] = 'required|array';
            $rules['Gallery.*'] = 'image|max:3000';
        }

        $dataValid = $this->runValidator($rules);
        if (!$dataValid['Valid']) {
            return $this->return_validator_error($dataValid);
        }

        // get current user
        $user = UserModel::find(decode_id(getallheaders()['Seth_UserID']));
        $this->load_model('Comment_model');

        // check if have comment for today in this post
        if ($this->comment_model->IsHaveCommentToday($post->ID, $user->ID)) {
            return response()->json(['error' => __('api.cmt_today_post')]);
        }

        // check if max comment for today
        if (UserLogCommentModel::IsMaxCommentToday($user->ID)) {
            return response()->json(['error' => __('api.max_cmt_today', ['today' => config('sethfood.MAX_COMMENT_PER_DAY')])]);
        }

        // ok add new comment.
        $comment = new CommentModel;
        $comment->UserID = $user->ID;
        $comment->PostID = $post->ID;
        $comment->Title = strip_tags($dataValid['Data']['Title']);
        $comment->Comment = strip_tags($dataValid['Data']['Comment']);
        $comment->Rating = $dataValid['Data']['Rating'];
        setObjLoggedInfo($comment, $user, true);

        // process upload
        if (isset($dataValid['Data']['Gallery'])) {
            $img = [];
            $up_path = config('sethfood.UPLOAD_ATTACHMENT_PATH');

            // upload now
            foreach ($dataValid['Data']['Gallery'] as $file) {
                if (!$file->isValid()) {
                    continue;
                }

                // process add img
                $image = Image::make($file);
                $new_name = "CMT_" . time() . "_" . md5($file->getClientOriginalName()) . rand(0,999999) . ".jpg";
                $full_path = $up_path . "/" . $new_name;

                // process to resize image
                $width = $image->width();
                $height = $image->height();
                if ($file->getSize() > 500) {
                    if ($width == $height) {
                        // square, so 1000x1000 will be good
                        $image->resize('1000', '1000');
                    } else if ($width > $height) {
                        // horizontal
                        $image->resize('1366', '768');
                    } else {
                        // vertical
                        $image->resize('705', '960');
                    }
                }

                // save
                $image->save(public_path($full_path), 70);
                $img[] = $full_path;
            }

            $comment->Gallery = implode(",", $img);
        }
        $comment->save();
        $comment->load(['User']);

        // log comment
        $log = new UserLogCommentModel;
        $log->UserID = $user->ID;
        $log->CommentID = $comment->ID;
        $log->CreatedDate = $comment->CreatedDate;
        $log->save();

        // return
        return response()->json(['data' => $comment->CommentData, 'msg' => __('api.add_cmt_success')]);
    }
}
