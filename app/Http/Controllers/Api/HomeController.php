<?php

namespace App\Http\Controllers\Api;

use App\Models\AttributeModel;
use App\Models\CategoryModel;
use App\Models\PostModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    CONST LIMIT = 6;
    //
    public function HomeData() {
        $result = [
            'categories' => [],
            'feature_posts' => [],
        ];
        // all cate
        $all_cate = CategoryModel::byActivate()->get();
        foreach ($all_cate as $cate) {
            $id = encode_id($cate->ID);
            $result['categories'][] = [
                'ID'    => $id,
                'Slug'  => $cate->Slug,
                'Name'  => $cate->Name,
                'id'    => $id,
                'text'  => $cate->Name,
                'TotalPost' => $cate->TotalPost
            ];
        }

        // feature places
        $this->load_model("Post_model");
        $result['feature_posts'] = $this->post_model->GetPopularPost(0, self::LIMIT);
        $result['attributes'] = AttributeModel::GetAllAttribute();

        // return data
        return response()->json($result);
    }
}
