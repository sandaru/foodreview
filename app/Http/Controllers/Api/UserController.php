<?php

namespace App\Http\Controllers\Api;

use App\Models\UserModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;

class UserController extends Controller
{

    public function GetByID($id) {
        $decode_id = decode_id($id);
        $user = UserModel::find($decode_id);
        if ($user === null) {
            return response()->json(['error' => __('api.user_not_found')]);
        }
        $this->load_model('Comment_model');

        $return_data = [
            'ID' => $id,
            'FullName'  => $user->FullName,
            'ProfileImage' => $user->ProfileImage,
            'TotalReview' => $user->TotalReview,
            'JoinDate' => Carbon::parse($user->CreatedDate)->format(config('sethfood.DATE_SHOW_FORMAT')),
            'AverageStar' => $this->comment_model->UserAverageStar($user->ID)
        ];

        return response()->json($return_data);
    }

    public function GetMembers() {
        $users = UserModel::all()->where('ID', '!=', $this->current_user()->ID);

        $result = [];
        foreach ($users as $user) {
            $result[] = [
                'id' => encode_id($user->ID),
                'text' => $user->FullName,
                'profile_image' => $user->ProfileImage
            ];
        }

        return response()->json($result);
    }

    public function UpdateProfile() {
        $rules = [
            'FirstName' => 'required|max:30',
            'LastName'  => 'required|max:30',
        ];

        if (request()->has('ProfileImage')) {
            $rules['ProfileImage_FILE'] = 'required|image|max:1000';
        }

        $dataValid = $this->runValidator($rules);
        if (!$dataValid['Valid']) {
            return $this->return_validator_error($dataValid);
        }

        // ok update user
        $user = $this->current_user();

        // update info
        $user->FirstName = $dataValid['Data']['FirstName'];
        $user->LastName = $dataValid['Data']['LastName'];

        // solving upload
        if (isset($dataValid['Data']['ProfileImage'])) {
            // get and check
            $file = $dataValid['Data']['ProfileImage'];
            if (!$file->isValid()) {
                return response()->json(['error' => __('api.upload_image_failed')]);
            }

            // upload down here
            $upload_path = config('sethfood.UPLOAD_AVATAR_PATH');
            // process add img
            $image = Image::make($file);
            $new_name = "USER_" . time() . "_" . md5($file->getClientOriginalName()) . rand(0,999999) . ".jpg";
            $full_path = $upload_path . "/" . $new_name;

            if ($file->getSize() > 300) {
                $image->resize('150', '150');
            }

            $image->save(public_path($full_path), 70);
            $user->ProfileImage = $full_path;
        }

        setObjLoggedInfo($user, $user);
        $user->save();

        $return_data = [
            'token' => $user->ApiToken,
            'user_id' => encode_id($user->ID),
            'first_name' => $user->FirstName,
            'last_name' => $user->LastName,
            'profile_image' => $user->ProfileImage,
        ];
        return response()->json(['msg' => __('api.updated_profile'), 'data' => $return_data]);
    }

    public function UpdatePassword() {
        $rules = [
            'Password' => 'required|min:6',
            'RetypePassword' => 'required|min:6|same:Password',
            'OldPassword' => 'required|min:6',
        ];

        $dataValid = $this->runValidator($rules);
        if (!$dataValid['Valid']) {
            return $this->return_validator_error($dataValid);
        }

        // ok update user
        $user = $this->current_user();

        if ($user->Password !== password_encrypt($dataValid['Data']['OldPassword'])) {
            return response()->json(['error' => __('api.old_pass_wrong')]);
        }

        $user->Password = password_encrypt(trim($dataValid['Data']['Password']));
        setObjLoggedInfo($user, $user);
        $user->save();

        return response()->json(['msg' => __('api.updated_password')]);
    }
}
