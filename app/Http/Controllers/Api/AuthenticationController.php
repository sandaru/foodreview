<?php

namespace App\Http\Controllers\Api;

use App\Models\UserModel;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;

class AuthenticationController extends Controller
{


    public function Login() {
        $check_method = $this->set_method("POST");
        if ($check_method !== true) {
            return $check_method;
        }

        $rules = [
            'Email' => 'required|email|exists:User,Email',
            'Password' => 'required|min:6'
        ];

        $dataValid = $this->runValidator($rules);
        if (!$dataValid['Valid']) {
            return $this->return_validator_error($dataValid);
        }

        // ok try to login here
        $user = UserModel::byEmail($dataValid['Data']['Email'])->first();

        // check password
        if (password_encrypt($dataValid['Data']['Password']) !== $user->Password) {
            return response()->json(['error' => __('api.wrong_password')]);
        }

        // ok logged - create a token
        $token = sha1(md5(time() . rand(0,999999) . $user->ID . $user->Email) . rand(0, 999999));
        $user->ApiToken = $token;
        $user->save(); // save token

        // return info
        $result = [
            'data' => [
                'token' => $token,
                'user_id' => Crypt::encryptString($user->ID),
                'first_name' => $user->FirstName,
                'last_name' => $user->LastName,
                'profile_image' => $user->ProfileImage,
            ],
            'msg' => __('api.login_success')
        ];
        return response()->json($result);
    }

    public function Register() {
        $check_method = $this->set_method("POST");
        if ($check_method !== true) {
            return $check_method;
        }

        $rules = [
            'Email'             => 'required|email|unique:User,Email',
            'Password'          => 'required|min:6',
            'FirstName'         => 'required|max:30',
            'LastName'          => 'required|max:30'
        ];

        $dataValid = $this->runValidator($rules);
        if (!$dataValid['Valid']) {
            return $this->return_validator_error($dataValid);
        }

        // ok register now
        $user = new UserModel;
        $user->Email = $dataValid["Data"]['Email'];
        $user->Password = password_encrypt($dataValid["Data"]['Password']);
        $user->FirstName = $dataValid["Data"]['FirstName'];
        $user->LastName = $dataValid["Data"]['LastName'];
        $user->CreatedByName = $user->ModifiedByName = $user->FirstName . " " .$user->LastName;
        $user->CreatedDate = $user->ModifiedDate = now();
        $user->CreatedByID = $user->ModifiedByID = 1;
        $user->save();

        // set id again ...
        $user->CreatedByID = $user->ModifiedByID = $user->ID;
        $user->save();

        // ok done.
        return response()->json(['msg' => __('api.register_success')]);
    }

    public function Forgot() {
        $check_method = $this->set_method("POST");
        if ($check_method !== true) {
            return $check_method;
        }

        $rules = [
            'Email' => 'required|email|exists:User,Email',
        ];

        $dataValid = $this->runValidator($rules);
        if (!$dataValid['Valid']) {
            return $this->return_validator_error($dataValid);
        }

        // create a token

        // send email to retrieve

        // ok
        return response()->json(['msg' => __('api.forgot_success')]);
    }

    public function RecheckAuth() {
        $headers = getallheaders();
        if (!isset($headers['Seth_Token']) ||  !isset($headers['Seth_UserID'])) {
            return response()->json(['error' => 'Not logged in']);
        }

        $user_id = decode_id($headers['Seth_UserID']);
        $token = $headers['Seth_Token'];

        $user = UserModel::FindTokenAndUserID($user_id, $token);
        if ($user == null) {
            return response()->json(['error' => 'Wrong information => Not logged in']);
        }

        return response()->json(['success' => 'ok']);
    }
}
