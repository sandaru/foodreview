<?php

namespace App\Http\Controllers\Api;

use App\Models\GroupMemberModel;
use App\Models\GroupModel;
use App\Models\UserModel;
use App\Models\VotePlaceModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GroupController extends Controller
{
    CONST LIMIT_VOTES = 10;

    public function GetUserGroup() {
        $headers = getallheaders();
        $user_id = decode_id($headers['Seth_UserID']);

        $allUserGroup = GroupModel::GetUserGroup($user_id);
        $result = [];
        foreach ($allUserGroup as $item) {
            $result[] = $item->CompiledData;
        }

        return response()->json($result);
    }

    public function GetByID($id) {
        $group = GroupModel::find(decode_id($id));
        if ($group == null) {
            return response()->json(['error' => __('api.group_not_existed')]);
        }

        $current_user = $this->current_user();
        // check if user in group or not
        if(!GroupMemberModel::IsUserInGroup($group->ID, $current_user->ID)) {
            // not in group
            return response()->json(['error' => __('api.not_in_group')]);
        }

        $return_data = $group->CompiledData;
        $return_data['IsAdmin'] = $current_user->ID == $group->AdminUserID;
        $return_data['Members'] = [];

        // get group member
        foreach ($group->GroupMembers as $mem) {
            $return_data['Members'][] = $mem->CompiledData;
        }

        // get vote newest vote of this group
        $return_data['Votes'] = [];
        $votes = VotePlaceModel::GetNewestVotePlace($group->ID, static::LIMIT_VOTES);
        foreach ($votes as $vote) {
            $return_data['Votes'][] = $vote->CompiledData;
        }

        return response()->json($return_data);
    }

    public function CreateGroup() {
        $rules = [
            'Name' => 'required|max:30',
            'Description' => 'required|max:100',
            'Members' => 'required|array|min:1'
        ];

        if (request()->has('GroupImage')) {
            $rules['GroupImage_FILE'] = "required|image|max:500";
        }

        $dataValid = $this->runValidator($rules);
        if (!$dataValid['Valid']) {
            return $this->return_validator_error($dataValid);
        }

        // get current user
        $current_user = $this->current_user();

        // ok create a new group
        $group = new GroupModel;
        $group->AdminUserID = $current_user->ID;
        $group->Name = $dataValid['Data']['Name'];
        $group->Description = $dataValid['Data']['Description'];
        setObjLoggedInfo($group, $current_user, true);

        // upload image here
        if (isset($dataValid['Data']['GroupImage']) && $dataValid['Data']['GroupImage']->isValid()) {
            $file = $dataValid['Data']['GroupImage'];

            // move file
            $path = public_path(config('sethfood.UPLOAD_GROUP_PATH'));
            $new_name = "GROUP_" . time() . "_" . md5($file->getClientOriginalName()) . rand(0,999999) . "." . $file->getClientOriginalExtension();
            $full_path = config('sethfood.UPLOAD_GROUP_PATH') . "/" . $new_name;
            $file->move($path, $new_name);

            // set value
            $group->GroupImage = $full_path;
        }
        $group->save();

        // ok now we add member into here
        // add admin
        GroupMemberModel::create(['GroupID' => $group->ID, 'UserID' => $current_user->ID]);

        // add users
        foreach ($dataValid['Data']['Members'] as $user_id) {
            $user = UserModel::find(decode_id($user_id));
            if (empty($user_id)) {
                continue;
            }

            // add
            GroupMemberModel::create(['GroupID' => $group->ID, 'UserID' => $user->ID]);
        }

        return response()->json(['success' => __('api.created_group')]);
    }
}
