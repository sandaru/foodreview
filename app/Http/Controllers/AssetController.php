<?php
/**
 * Created by PhpStorm.
 * User: Seth Phat
 * Date: 8/14/2018
 * Time: 10:11 PM
 */

namespace App\Http\Controllers;


use Illuminate\Support\Facades\View;

class AssetController extends Controller
{
    public function getAsset($type, $name) {
        $content_type = "";
        switch ($type) {
            case "js":
                $content_type = "application/javascript";
                break;
            case "css":
                $content_type = "text/css";
                break;
        }
        $detail_asset = View::make("assets.$type.$name");

        return response($detail_asset, 200,[
            'Content-Type' => $content_type
        ]);
    }
}