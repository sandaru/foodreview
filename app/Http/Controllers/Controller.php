<?php

namespace App\Http\Controllers;

use App\Models\UserModel;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    /**
     * Fast validator running (POST and GET ONLY)
     * @param array $arrInfo Rules ([Field => Value])
     * @param string $type (GET or POST)
     * @param array moreData More data to populate in main data
     * @return array
     */
    protected function runValidator($arrInfo, $type = "POST", $moreData = [])
    {
        $request = request();
        $data = [];
        $returnArr = [
            'Valid'     => true,
        ];

        // retrieve data
        foreach ($arrInfo as $key => $value)
        {
            if ($type == "POST")
            {
                // array of files
                if (stristr($key,'.*')) {
                    continue;
                }
                else if (stristr($key, "_FILE")) {
                    $new_key = str_replace("_FILE", "", $key);
                    $data[$new_key] = $request->file($new_key);

                    // remove old value in arrInfo
                    $old = $arrInfo[$key];
                    unset($arrInfo[$key]);
                    $arrInfo[$new_key] = $old;
                }
                else {
                    $data[$key] = $request->post($key);
                }
            }
            else {
                $data[$key] = $request->get($key);
            }
        }

        // populate more data
        $data = array_merge($data, $moreData);

        // Valid now
        $valid = Validator::make($data, $arrInfo);

        if ($valid->fails()) {
            $returnArr['Valid'] = false;
            $returnArr['Errors'] = $valid;
        } else {
            $returnArr['Data'] = $data;
        }

        return $returnArr;
    }

    /**
     * Return json validator error
     * @param $dataValid
     */
    protected function return_validator_error($dataValid) {
        return response()->json(['error' => $dataValid['Errors']->errors()->first()]);
    }

    /**
     * set method
     * @param string $type
     */
    protected function set_method($type = 'get') {
        if (!request()->isMethod($type)) {
            return response()->json(['error' => 'Method not allowed'], 405);
        }

        return true;
    }

    protected function load_model($model_name, $variable_name = null) {
        if ($variable_name == null) {
            $variable_name = strtolower($model_name);
        }
        if (!file_exists(app_path('QueryModels/' . $model_name . '.php'))) {
            throw new \Exception("Query Model $model_name failed to load!");
        }

        $class_path = "App\\QueryModels\\$model_name";
        $this->{$variable_name} = new $class_path();
    }

    protected function current_user() {
        $header = getallheaders();
        if (!isset($header['Seth_UserID'])) {
            return null;
        }

        return UserModel::find(decode_id($header['Seth_UserID']));
    }
}
