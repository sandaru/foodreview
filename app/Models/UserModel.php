<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserModel extends Model
{
    protected $table = "User";
    protected $primaryKey = "ID";
    public $timestamps = false;
    public static $snakeAttributes = false;

    // accessor...
    public function getFullNameAttribute() {
        return "{$this->FirstName} {$this->LastName}";
    }
    public function getTotalReviewAttribute() {
        return $this->hasMany('App\Models\CommentModel', 'UserID')
                    ->where('Verified', 1)
                    ->count();
    }

    // relationship

    // scope
    public function scopeByEmail($query, $email) {
        return $query->where('Email', $email);
    }


    /**
     * Find token & user id
     * @param $user_id
     * @param $token
     */
    public static function FindTokenAndUserID($user_id, $token) {
        $info = self::query()->where('ID', $user_id)
                            ->where('ApiToken', $token)
                            ->first();

        return $info;
    }
}
