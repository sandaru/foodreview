<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class VotePlaceItemEntryModel extends Model
{
    protected $table = "VotePlaceItemEntry";
    protected $primaryKey = null;
    public $incrementing = false;
    public $timestamps = false;
    public static $snakeAttributes = false;
    protected $fillable = ['UserID', 'VotePlaceItemID', 'CreatedDate'];

    public function User() {
        return $this->belongsTo(UserModel::class, "UserID");
    }

    public function VotePlaceItem() {
        return $this->belongsTo(VotePlaceItemModel::class, "VotePlaceItemID");
    }

    public function getCompiledDataAttribute() {
        return [
            'ID' => encode_id($this->UserID),
            'FullName' => $this->User->FullName,
            'ProfileImage' => $this->User->ProfileImage,
            'CreatedDate' => Carbon::parse($this->CreatedDate)->format("d/m/Y H:i:s")
        ];
    }

    public static function IsVoted($user_id, $item_id) {
        $qr = self::query()->where('UserID', $user_id)
                            ->where('VotePlaceItemID', $item_id)
                            ->count();

        return $qr > 0;
    }

    public static function RemoveVote($user_id, $item_id)
    {
        $qr = self::query()->where('UserID', $user_id)
            ->where('VotePlaceItemID', $item_id);

        return $qr->delete();
    }
}
