<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AttributeModel extends Model
{
    protected $table = "Attribute";
    protected $primaryKey = "ID";
    public $timestamps = false;
    public static $snakeAttributes = false;


    public static function GetAttributeName($list_ids) {
        $info = self::query()->whereIn('ID', $list_ids)->pluck('Name');

        return $info;
    }

    public static function GetAllAttribute() {
        $result = self::all();

        $data = [];
        foreach ($result as $item) {
            $data[] = [
                'id' => encode_id($item->ID),
                'text' => $item->Name,
            ];
        }

        return $data;
    }
}
