<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryModel extends Model
{
    protected $table = "Category";
    protected $primaryKey = "ID";
    public $timestamps = false;
    public static $snakeAttributes = false;

    public function getTotalPostAttribute() {
        return $this->hasMany('App\Models\PostModel', 'CategoryID')->count();
    }

    public function scopeByActivate($query) {
        return $query->where('Activate', 'yes');
    }

    public function scopeBySlug($query, $slug) {
        return $query->where('Slug', $slug);
    }

    // mutators
    public function setUniqueSlugAttribute($value) {
        $this->Slug = generate_unique_slug("Category", "Slug", $value);
    }

    // accessor
    public function getCompiledDataAttribute() {
        return [
            'ID' => encode_id($this->ID),
            'Slug' => $this->Slug,
            'Name' => $this->Name
        ];
    }
}
