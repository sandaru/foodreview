<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VotePlaceItemModel extends Model
{
    protected $table = "VotePlaceItem";
    protected $primaryKey = "ID";
    public $timestamps = false;
    public static $snakeAttributes = false;
    protected $fillable = ['PostID', 'VotePlaceID'];

    public function Post() {
        return $this->belongsTo(PostModel::class, "PostID");
    }

    public function Vote() {
        return $this->belongsTo(VotePlaceModel::class, "VotePlaceID");
    }

    public function Entries() {
        return $this->hasMany(VotePlaceItemEntryModel::class, "VotePlaceItemID");
    }

    public function getCompiledDataAttribute() {
        return [
            'ID' => encode_id($this->ID),
            'TotalVote' => $this->Entries()->count(),
            'Post' => [
                'ID' => $this->Post->ID,
                'Slug' => $this->Post->Slug,
                'Title' => $this->Post->Title,
                'Address' => $this->Post->Address,
                'Image' => empty($this->Post->GalleryItem) ? null : $this->Post->GalleryItem[0]
            ],
        ];
    }

    public function delete()
    {
        $this->Entries()->delete();
        return parent::delete();
    }
}
