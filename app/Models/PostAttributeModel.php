<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostAttributeModel extends Model
{
    protected $table = "PostAttribute";
//    protected $primaryKey = "ID";
    public $timestamps = false;
    public static $snakeAttributes = false;

    public function Attribute() {
        $this->belongsTo('App\Models\AttributeModel', 'AttributeID');
    }
}
