<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GroupMemberModel extends Model
{
    protected $table = "GroupMember";
    protected $primaryKey = null;
    public $incrementing = false;
    public $timestamps = false;
    public static $snakeAttributes = false;
    protected $fillable = ['GroupID', 'UserID'];

    public function User() {
        return $this->belongsTo(UserModel::class, "UserID");
    }

    public function getCompiledDataAttribute() {
        return [
            'ID' => encode_id($this->User->ID),
            'FullName' => $this->User->FullName,
            'ProfileImage' => $this->User->ProfileImage
        ];
    }

    public static function IsUserInGroup($group_id, $user_id) {
        $total = self::query()->where('GroupID', $group_id)
                            ->where('UserID', $user_id)
                            ->count();

        return $total > 0;
    }
}
