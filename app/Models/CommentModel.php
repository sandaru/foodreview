<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CommentModel extends Model
{
    protected $table = "Comment";
    protected $primaryKey = "ID";
    public $timestamps = false;
    public static $snakeAttributes = false;

    // rela
    public function User() {
        return $this->belongsTo('App\Models\UserModel', 'UserID');
    }

    // accessor
    public function getGalleryItemAttribute() {
        return empty($this->Gallery) ? [] : explode(",", $this->Gallery);
    }

    public function getCommentDataAttribute() {
        $created_date = Carbon::parse($this->CreatedDate);

        return [
            'ID' => encode_id($this->ID),
            'Title' => $this->Title,
            'Comment' => nl2br($this->Comment),
            'Gallery' => $this->GalleryItem,
            'Rating' => $this->Rating,
            'CreatedDate' => $created_date->format(config('sethfood.DATETIME_SHOW_FORMAT')),
            'CreatedDateString' => $created_date->diffForHumans(),

            // user comment
            'User' => [
                'ID'    => encode_id($this->User->ID),
                'FirstName' => $this->User->FirstName,
                'LastName' => $this->User->LastName,
                'ProfileImage' => $this->User->ProfileImage,
                'TotalReview' => $this->User->TotalReview
            ],
        ];
    }

    // static funcs

    public static function GetCommentByPostID($post_id, $start = 0, $limit = 10) {
        $comments = self::query()
                            ->where('PostID', $post_id)
                            ->where('Verified', 1)
                            ->orderBy('CreatedDate', 'DESC')
                            ->take($limit)
                            ->skip($start)
                            ->get();

        $result = [];
        Carbon::setLocale('vi');
        foreach ($comments as $cmt) {
            $result[] = $cmt->CommentData;
        }

        return $result;
    }

    public static function GetStatistic($post_id) {
        $info = DB::select("
        SELECT COUNT(*) as TotalComment, ROUND(AVG(Comment.Rating), 1) as Overall
        FROM Comment
        WHERE PostID = ? AND Verified = 1
        ", [$post_id]);

        $first = $info[0];
        return [
            'Overall' => empty($first->Overall) ? 0 : $first->Overall,
            'TotalComment' => $first->TotalComment
        ];
    }
}
