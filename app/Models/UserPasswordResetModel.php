<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserPasswordResetModel extends Model
{
    protected $table = "UserPasswordReset";
    protected $primaryKey = "Email";
    public $timestamps = false;
    public static $snakeAttributes = false;
}
