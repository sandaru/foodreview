<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GroupModel extends Model
{
    protected $table = "Group";
    protected $primaryKey = "ID";
    public $timestamps = false;
    public static $snakeAttributes = false;

    public function GroupMembers() {
        return $this->hasMany('App\Models\GroupMemberModel', 'GroupID');
    }

    public function scopeByActivate($query) {
        return $query->where('Activate', true);
    }

    public function scopeByUserID($query, $user_id) {
        return $query->with('GroupMembers')
                    ->whereHas('GroupMembers', function ($qr) use ($user_id) {
                        $qr->where('UserID', $user_id);
                    });
    }

    public function getCompiledDataAttribute() {
        return [
            'ID' => encode_id($this->ID),
            'Name'  => $this->Name,
            'Description'  => $this->Description,
            'GroupImage'  => $this->GroupImage,
            'TotalMember' => $this->GroupMembers()->count()
        ];
    }

    public static function GetUserGroup($user_id) {
        $qr = GroupModel::query()
                            ->byUserID($user_id)
                            ->byActivate()
                            ->orderBy('CreatedDate', 'DESC');
        return $qr->get();
    }
}
