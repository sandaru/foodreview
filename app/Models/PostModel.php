<?php

namespace App\Models;

use function DeepCopy\deep_copy;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PostModel extends Model
{
    protected $table = "Post";
    protected $primaryKey = "ID";
    public $timestamps = false;
    public static $snakeAttributes = false;

    // relationship
    public function Comments() {
        return $this->hasMany('App\Models\CommentModel', 'PostID');
    }
    public function PostAttributes() {
        return $this->hasMany('App\Models\PostAttributeModel', 'PostID');
    }

    public function PostItems() {
        return $this->hasMany('App\Models\PostItemModel', 'PostID');
    }

    public function Category() {
        return $this->belongsTo('App\Models\CategoryModel', 'CategoryID');
    }

    // scope
    public function scopeBySlug($query, $slug) {
        return $query->where('Slug', $slug);
    }
    public function scopeByActivated($query) {
        return $query->where('Activate', true);
    }
    public function scopeByVerified($query) {
        return $query->where('Verified', true);
    }

    // mutators
    public function setUniqueSlugAttribute($value) {
        $this->Slug = generate_unique_slug("Post", "Slug", $value);
    }

    // accessor
    public function getGalleryItemAttribute() {
        return empty($this->Gallery) ? [] : explode(",", $this->Gallery);
    }
    public function getCompiledDataAttribute() {
        $statistic = CommentModel::GetStatistic($this->ID);
        $attributes = AttributeModel::GetAttributeName($this->PostAttributes->pluck('AttributeID'));
        $items = [];
        foreach ($this->PostItems as $postItem) {
            $items[] = $postItem->CompiledData;
        }

        // return data
        return [
            'ID'                => encode_id($this->ID),
            'Slug'              => $this->Slug,
            'Title'             => $this->Title,
            'Description'      => nl2br($this->Description),
            'Address'           => $this->Address,
            'ShortDescription'  => $this->ShortDescription,
            'Phone'             => $this->Phone,
            'Website'             => $this->Website,
            'PrepareTime'         => $this->PrepareTime,
            'MinPrice'           => $this->MinPrice,
            'MaxPrice'           => $this->MaxPrice,
            'Gallery'           => $this->GalleryItem,
            'Overall'            => $statistic['Overall'],
            'TotalComment'      => $statistic['TotalComment'],
            'Category'          => [
                'ID'            => encode_id($this->Category->ID),
                'Name'          => $this->Category->Name
            ],
            'Attributes'         => $attributes,
            'Items'             => $items
        ];
    }

    // function goes down here
    public function delete()
    {
        // delete child
        $this->PostItems()->delete();
        $this->PostAttributes()->delete();
        return parent::delete(); // delete current.
    }
}
