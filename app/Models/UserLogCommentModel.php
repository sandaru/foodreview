<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserLogCommentModel extends Model
{
    protected $table = "UserLogComment";
    protected $primaryKey = "ID";
    public $timestamps = false;
    public static $snakeAttributes = false;
    public $fillable = [
        'UserID',
        'CommentID',
        'CreatedDate'
    ];


    public static function IsMaxCommentToday($user_id) {
        $today = now()->format('Y-m-d');
        $query = self::query()
                        ->where('UserID', $user_id)
                        ->whereBetween('CreatedDate', [$today . " 00:00:00", $today . " 23:59:59"]);
        $total = $query->count();

        return $total >= config('sethfood.MAX_COMMENT_PER_DAY'); // 11 >= 10 => max today lol
    }
}
