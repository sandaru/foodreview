<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostItemModel extends Model
{
    protected $table = "PostItem";
    protected $primaryKey = "ID";
    public $timestamps = false;
    public static $snakeAttributes = false;


    public function getCompiledDataAttribute() {
        return [
            'ID' => encode_id($this->ID),
            'Title'  => $this->Title,
            'Description'  => $this->Description,
            'Image'  => $this->Image,
            'Price'  => $this->Price,
        ];
    }
}
