<?php

namespace App\Jobs;

use App\Mail\VoteEmailCreated;
use App\Models\GroupModel;
use App\Models\VotePlaceModel;
use App\QueryModels\ModelManager;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SendEmailForVote implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $group_id;
    private $vote_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($group_id, $vote_id)
    {
        $this->group_id = $group_id;
        $this->vote_id = $vote_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // TODO: Send email to all user in this group and notify them about the vote :D
        if (config('sethfood.SEND_EMAIL_NEW_VOTE') === true) {
            // get all email in this group
            $groupMemberModel = ModelManager::getInstance()->getModel('GroupMember');
            $allMemberInGroup = $groupMemberModel->getAllUserEmailInGroup($this->group_id);

            // prepare data
            $groupInfo = GroupModel::find($this->group_id);
            $voteInfo = VotePlaceModel::find($this->vote_id);

            foreach ($allMemberInGroup as $mem_info) {
                // send email here
                Mail::to($mem_info->Email)
                    ->send(new VoteEmailCreated($groupInfo, $voteInfo, $mem_info->FullName));
            }
        }
    }
}
