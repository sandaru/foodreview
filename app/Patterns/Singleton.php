<?php
/**
 * Created by PhpStorm.
 * User: Seth Phat
 * Date: 8/17/2018
 * Time: 10:25 PM
 */

namespace App\Patterns;


class Singleton
{
//    protected static $instance = null;

    public static function getInstance()
    {
        static $instance = null;

        if ($instance === null) {
            $instance = new static;
        }

        return $instance;
    }

    protected function __construct()
    {
    }

    protected function __clone()
    {
    }
}