<?php
/**
 * Created by PhpStorm.
 * User: Seth Phat
 * Date: 8/21/2018
 * Time: 8:43 PM
 */

namespace App\QueryModels;


use Illuminate\Support\Facades\DB;

class VotePlaceItem_model
{
    public function getCurrentItemSlug($vote_id) {
        $qr = DB::table("VotePlaceItem")
                        ->where("VotePlaceID", $vote_id)
                        ->join("Post", "Post.ID", "=", "VotePlaceItem.PostID")
                        ->selectRaw("VotePlaceItem.ID as ItemID, Post.Slug, Post.ID as PostID")
                        ->get();

        return $qr;
    }
}