<?php
/**
 * Created by PhpStorm.
 * User: Seth Phat
 * Date: 8/17/2018
 * Time: 9:07 PM
 */

namespace App\QueryModels;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class VotePlaceItemEntry_model
{
    public function GetPopularVoteItem($vote_place_id) {
        $qr = DB::table("VotePlaceItem")
            ->join("VotePlace", "VotePlaceItem.VotePlaceID", "=", "VotePlace.ID")
            ->leftJoin("VotePlaceItemEntry", "VotePlaceItemEntry.VotePlaceItemID", "=", "VotePlaceItem.ID")
            ->selectRaw("COUNT(VotePlaceItemEntry.UserID) as TotalVote, VotePlaceItem.ID as PlaceItemID")
            ->where("VotePlace.ID", $vote_place_id)
            ->groupBy("VotePlaceItem.ID")
            ->orderBy("TotalVote", "DESC");

        return $qr->get()->first();
    }
}