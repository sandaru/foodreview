<?php
/**
 * Created by PhpStorm.
 * User: 84066
 * Date: 7/27/2018
 * Time: 12:13 PM
 */


namespace App\QueryModels;
use Illuminate\Support\Facades\DB;

class Comment_model
{
    public function IsHaveCommentToday($post_id, $user_id) {
        $today = now()->format('Y-m-d');
        $total = DB::table("Comment")
                    ->where('PostID', $post_id)
                    ->where('UserID', $user_id)
                    ->whereBetween('CreatedDate', [$today . " 00:00:00", $today . " 23:59:59"])
                    ->count();

        return $total > 0;
    }

    public function UserAverageStar($user_id) {
        $result = DB::table("Comment")
                    ->where('UserID', $user_id)
                    ->where('Verified', 1)
                    ->selectRaw("ROUND(AVG(Rating), 1) as Rating")
                    ->get();

        return empty($result[0]->Rating) ? 0 : $result[0]->Rating;
    }
}