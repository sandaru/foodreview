<?php
/**
 * Created by PhpStorm.
 * User: Seth Phat
 * Date: 7/20/2018
 * Time: 9:26 PM
 */

namespace App\QueryModels;
use Illuminate\Support\Facades\DB;

class Post_model
{
    protected $table = "Post";
    protected $primaryKey = "ID";


    /**
     * Query for search post
     * @param string $keyword
     * @param null $cate_id
     * @param int $start
     * @param int $limit
     * @return array
     */
    public function SearchPost($keyword = "", $cate_id = null, $start = 0, $limit = 10) {
        $query = DB::table("Post")
            ->selectRaw('Post.*, 
                                                 Category.Name as CategoryName, 
                                                 ROUND(AVG(Comment.Rating), 1) as Overall, 
                                                 COUNT(Comment.ID) as TotalComment')
            ->skip($start)
            ->take($limit)
            ->leftJoin("Comment", function ($join) {
                $join->on('Comment.PostID', '=', 'Post.ID')
                    ->on('Comment.Verified', '=', DB::raw("1"));
            })
            ->join("Category", 'Post.CategoryID', '=', 'Category.ID')
            ->groupBy('Post.ID')
            ->orderBy('Post.CreatedDate', 'DESC')
            ->where(function ($query) use ($keyword) {
                $query->where('Post.Title', 'LIKE', "%$keyword%");
                $query->orWhere('Post.Description', 'LIKE', "%$keyword%");
            })
            ->where('Post.Activate', '1')
            ->where('Post.Verified', '1');

        // query for total result
        $total_query =  DB::table("Post")
            ->where(function ($query) use ($keyword) {
                $query->where('Post.Title', 'LIKE', "%$keyword%");
                $query->orWhere('Post.Description', 'LIKE', "%$keyword%");
            })
            ->where('Post.Activate', '1')
            ->where('Post.Verified', '1');

        if (!empty($cate_id)) {
            $cate_id = decode_id($cate_id);
            $query->where('CategoryID', $cate_id);
            $total_query->where('CategoryID', $cate_id);
        }

        // get total result
        $total_result = $total_query->count("Post.ID");

        // get result data
        $result = $query->get();

        // compile data
        $data = [
            'TotalResult' => $total_result,
            'Data'  => [],
        ];
        foreach ($result as $post) {
            $data['Data'][] = [
                'ID'                => encode_id($post->ID),
                'Title'             => $post->Title,
                'Slug'             => $post->Slug,
                'Description'      => $post->Description,
                'Address'           => $post->Address,
                'Website'           => $post->Website,
                'Phone'             => $post->Phone,
                'MinPrice'           => $post->MinPrice,
                'MaxPrice'           => $post->MaxPrice,
                'Gallery'           => empty($post->Gallery) ? [] : explode(",", $post->Gallery),
                'Overall'            => empty($post->Overall) ? 0 : $post->Overall,
                'TotalComment'      => $post->TotalComment,
                'CategoryName'      => $post->CategoryName,
                'Category'          => [
                    'ID'            => encode_id($post->CategoryID),
                    'Name'          => $post->CategoryName
                ]
            ];
        }

        return $data;
    }


    /**
     * Get popular post for home page
     * @param int $limit
     * @return array
     */
    public static function GetPopularPost($start = 0, $limit = 6) {
        $result = DB::select('
                  SELECT Post.*, Category.Name as CategoryName, ROUND(AVG(IFNULL(Comment.Rating, 0)), 1) as Overall, COUNT(Comment.ID) as TotalComment 
                  FROM Post 
                  JOIN Comment on Post.ID = Comment.PostID AND Comment.Verified = 1
                  JOIN Category on Post.CategoryID = Category.ID
                  WHERE Post.Activate = 1 AND Post.Verified = 1
                  GROUP BY Post.ID
                  ORDER BY Overall DESC LIMIT ? OFFSET ?', [$limit, $start]);

        $data = [];
        foreach ($result as $row) {
            $data[] = [
                'ID' => encode_id($row->ID),
                'Title' => $row->Title,
                'Slug' => $row->Slug,
                'Address' => $row->Address,
                'Website' => $row->Website,
                'Phone' => $row->Phone,
                'Gallery' => empty($row->Gallery) ? [] : explode(",", $row->Gallery),
                'TotalComment' => $row->TotalComment,
                'Overall' => $row->Overall,
                'CategoryName' => $row->CategoryName
            ];
        }

        return $data;
    }
}