<?php
/**
 * Created by PhpStorm.
 * User: Seth Phat
 * Date: 8/17/2018
 * Time: 10:25 PM
 */

namespace App\QueryModels;


use App\Patterns\Singleton;

class ModelManager extends Singleton
{
    private $path = "\App\QueryModels\\";

    protected $model_group = [];
    public function getModel($model_name) {
        $class = $this->path . $model_name . "_model";

        if (!isset($this->model_group[$model_name])) {
            $this->model_group[$model_name] = new $class();
        }

        return $this->model_group[$model_name];
    }
}