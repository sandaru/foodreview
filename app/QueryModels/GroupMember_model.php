<?php
/**
 * Created by PhpStorm.
 * User: Seth Phat
 * Date: 8/19/2018
 * Time: 11:58 AM
 */

namespace App\QueryModels;


use Illuminate\Support\Facades\DB;

class GroupMember_model
{
    public function getAllUserEmailInGroup($group_id) {
        $qr = DB::table("GroupMember")
                        ->join("Group", "Group.ID", "=", "GroupMember.GroupID")
                        ->join("User", "User.ID", "=", "GroupMember.UserID")
                        ->where("GroupID", $group_id)
                        ->selectRaw("User.Email, CONCAT(User.FirstName, ' ', User.LastName) as FullName");

        return $qr->get();
    }
}