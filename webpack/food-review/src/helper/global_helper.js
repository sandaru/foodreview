import {SiteConfig} from "config/config";

var Helper = {};

Helper.getUserImage = function (img) {
    if (!img) {
        return SiteConfig.resource_url + "/images/default-avatar.png";
    } else {
        return SiteConfig.resource_url + img;
    }
};

Helper.getAttachmentImage = function (img) {
    if (!img) {
        return SiteConfig.resource_url + "/images/default_attachment.png";
    } else {
        return SiteConfig.resource_url + img;
    }
};

Helper.getGroupImage = function(img) {
    if (!img) {
        return SiteConfig.resource_url + "/images/default-group.png";
    } else {
        return SiteConfig.resource_url + img;
    }
};

Helper.getUploadAjaxSetting = function() {
    return {
        enctype: 'multipart/form-data',
        contentType: false,
        processData: false
    }
};

Helper.isImage = function(file) {
    if (file instanceof File && file.type.indexOf("image") >= 0) {
        return true;
    }
    return false;
};

Helper.ratingToClass = function (rating) {
    if (rating >= 4) {
        return "featured-rating-green";
    } else if (rating >= 2 && rating < 4) {
        return "featured-rating-orange";
    } else {
        return "featured-rating-red";
    }
};

export {
    Helper
}