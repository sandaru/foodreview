import {SiteConfig} from "config/config";

var RequestService = function () {
    var ins = {};
    var api_url = SiteConfig.api_url;
    var controller = "";

    ins.setController = (name) => {
        controller = name;
        return ins;
    };

    var getURL = (actionName) => {
        return api_url + "/" + controller + "/" + actionName;
    };

    ins.get = (actionName, data) => {
        // return axios.get(getURL(actionName), data).then(result => result).catch(err => {
        //     SethPhatToaster.error("Server error.");
        // });
        return AjaxService.get(getURL(actionName), data)
            .fail(err => {
                SethPhatToaster.error("Server error.");
            });
    };

    ins.post = (actionName, data, settings) => {
        // return axios.post(getURL(actionName), data, settings).then(result => result).catch(err => {
        //     SethPhatToaster.error("Server error.");
        // });
        return AjaxService.post(getURL(actionName), data, settings)
            .fail(err => {
                SethPhatToaster.error("Server error.");
            });
    };

    return ins;
}();

export {
    RequestService
}
