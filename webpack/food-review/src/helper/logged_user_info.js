var LoggedUserInfo = function () {
    var ins = {};

    var user_info = null;
    let store_key = 'LoggedUserInfo';

    ins.setInfo = function (info) {
        user_info = info;
        // set into the localStorage too
        localStorage.setItem(store_key, JSON.stringify(info));
        // set ajax
        ins.setAjaxHeader();
    };

    ins.getInfo = function () {
        return user_info;
    };

    ins.retrieveStorage = function() {
        var data = localStorage.getItem(store_key);
        if (data == null) {
            return false;
        }

        var parsed_info = null;
        try {
            parsed_info = JSON.parse(data);
        } catch (e) {
            return false;
        }

        ins.setInfo(parsed_info);
        return true;
    };

    ins.isLoggedIn = function () {
        return user_info !== null;
    };

    ins.setAjaxHeader = function () {
        $.ajaxSetup({
            headers: {
                'Seth_UserID': user_info.user_id,
                'Seth_Token': user_info.token,
            }
        });
    };

    ins.clearAjax = function() {
        $.ajaxSetup({
            beforeSend: function (xhr){}
        });
    };

    ins.logout = function () {
        localStorage.removeItem(store_key);
        user_info = null;
        ins.clearAjax();
    };

    return ins;
}();

export {
    LoggedUserInfo
}