import * as types from './mutation-types'

export const mutations = {
    // LOL
    [types.SET_CATEGORY_KEY] (state, cates)
    {
        state.categories = cates;
    },

    // user types
    [types.LOGGED_USER] (state, info) {
        state.logged_user = info;
    },

    [types.FEATURE_POST] (state, posts) {
        state.feature_posts = posts;
    },

    [types.ATTRIBUTE_POST] (state, attrs) {
        state.attributes = attrs;
    }
};
