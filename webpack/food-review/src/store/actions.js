import * as types from './mutation-types'

// category
export const setCategory = ({commit}, data) => {
    commit(types.SET_CATEGORY_KEY, data);
};

export const setLoggedUser = ({commit}, data) => {
    commit(types.LOGGED_USER, data);
};

export const setFeaturePost = ({commit}, data) => {
    commit(types.FEATURE_POST, data);
};

export const setAttribute = ({commit}, data) => {
    commit(types.ATTRIBUTE_POST, data);
};