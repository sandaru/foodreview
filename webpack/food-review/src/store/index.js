import Vue from 'vue';
import Vuex from 'vuex';
import {mutations} from "./mutations";
import * as actions from './actions';


Vue.use(Vuex);

const state = {
    categories: [],
    logged_user: null,
    feature_posts: [],
    attributes: [],
};

export default new Vuex.Store({
    state,
    mutations,
    actions
});
