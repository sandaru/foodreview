import Vue from 'vue'
import App from './App.vue'
import {router} from "./config/router";
import axios from 'axios';
import store from './store';
import {RequestService} from "./helper/request_service";
import 'jquery';
import 'popper.js';
import 'bootstrap';
import * as _ from 'underscore';
import {SethPhatToaster} from "./helper/toaster";
import {AjaxService} from "./helper/ajax_service";
import {LoggedUserInfo} from "./helper/logged_user_info";
import {SiteConfig} from "./config/config";
import Vue2Filters from 'vue2-filters';
import {DateTimePickerBuilder} from "./helper/init_datetimepicker";

// Axios globally
window.axios = axios;
window._ = _;
window.RequestService = RequestService;
window.SethPhatToaster = SethPhatToaster;
window.$ = window.jQuery = require('jquery');
window.AjaxService = AjaxService;
window.moment = require('moment');
window.setSiteName = function (name) {
    if (_.isUndefined(name) || name === null || name === "") {
        document.title = SiteConfig.app_name;
        return;
    }
    document.title = name + " - " + SiteConfig.app_name;
};

// impot css
import 'assets/css/featherlight.min.css';
import 'assets/css/input-char-counter.css';
import 'pc-bootstrap4-datetimepicker/build/css/bootstrap-datetimepicker.css';

// required normal es5 libs..
require('assets/js/jquery.noty.packaged');
require('assets/js/featherlight.min');
require('assets/js/input-char-counter');

// run config
AjaxService.bindAjaxLoading();
LoggedUserInfo.retrieveStorage();
DateTimePickerBuilder.build();

// Vue use components
Vue.use(Vue2Filters);

// event for vue component :(
export const eventBus = new Vue();

// Vue create...
new Vue({
    el: '#app',
    render: h => h(App),
    router,
    store
});
