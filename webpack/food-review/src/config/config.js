import * as _ from 'underscore';

var SiteConfig = {
    app_name: "B2BE Food Review",

    // api config
    api_url: _.clone(SethPhatFoodInfo.api_url), // no slash at the end

    // resource url (image, attachment,...)
    resource_url: _.clone(SethPhatFoodInfo.resource_url),

    // limit config
    get_review_limit: 5,
    search_limit: 12,

    // social config
    social_network: {
        facebook_url: "https://www.facebook.com/SethSandaru",
        instagram_url: "https://www.instagram.com/sethsandaru/",
        website_url: "http://sethphat.com",
        twitter_url: "",
    },

};

window.APP_NAME = SiteConfig.app_name;

export {
  SiteConfig
}
