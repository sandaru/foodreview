// important library
import Vue from 'vue'
import VueRouter from 'vue-router';
import MainPage from 'components/page/Home/MainPage';
import AboutUsPage from 'components/page/Home/AboutUsPage';
import PostDetailPage from 'components/page/Detail/PostDetailPage';
import FeaturePage from 'components/page/Explore/FeaturePage';
import NewPostPage from 'components/page/Explore/NewPostPage';
import CategoryPage from 'components/page/Explore/CategoryPage';
import SearchPage from 'components/page/Explore/SearchPage';
import AddPage from 'components/page/Add/AddPage';
import SettingPage from 'components/page/Profile/SettingPage';
import ProfilePage from 'components/page/Profile/ProfilePage';
import MyGroupPage from 'components/page/Group/MyGroupPage';
import GroupInfoPage from 'components/page/Group/GroupInfoPage';
import VoteInfoPage from 'components/page/Vote/VoteInfoPage';

// define route
Vue.use(VueRouter);
export const router = new VueRouter({
    routes: [
        {
            path: "/",
            name: "home",
            component: MainPage,
        },
        {
            path: "/Food-Detail/:id",
            name: "food-detail",
            component: PostDetailPage
        },
        {
            path: "/Feature",
            name: "feature",
            component: FeaturePage
        },
        {
            path: "/New",
            name: "new",
            component: NewPostPage
        },
        {
            path: "/Category/:id",
            name: "category",
            component: CategoryPage
        },
        {
            path: "/About",
            name: 'about',
            component: AboutUsPage
        },
        {
            path: "/Search",
            name: "search",
            component: SearchPage
        },
        {
            path: "/AddLocation",
            name: "add",
            component: AddPage
        },
        {
            path: "/Setting",
            name: "setting",
            component: SettingPage
        },
        {
            path: "/Profile/:id",
            name: "profile",
            component: ProfilePage
        },
        {
            path: "/MyGroup",
            name: "mygroup",
            component: MyGroupPage
        },
        {
            path: "/GroupInfo/:id",
            name: "groupinfo",
            component: GroupInfoPage
        },
        {
            path: "/VoteInfo/:id",
            name: "voteinfo",
            component: VoteInfoPage
        }
    ],
    scrollBehavior (to, from, savedPosition) {
        return { x: 0, y: 0 }
    }
});
