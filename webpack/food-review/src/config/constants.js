let EMIT_CONSTANT = {
    ADD_REVIEW: "AddReview",
};

let PREPARE_TIME_OPTIONS = [
    {
        id: 1,
        text: "Dưới 5 phút",
    },
    {
        id: 2,
        text: "Từ 5 - 10 phút",
    },
    {
        id: 3,
        text: "Từ 10 - 20 phút",
    },
    {
        id: 4,
        text: "Từ 20 - 30 phút",
    },
    {
        id: 5,
        text: "Từ 30 phút trở lên",
    }
];

export {
    EMIT_CONSTANT,
    PREPARE_TIME_OPTIONS
}