// important library
import Vue from 'vue'
import VueRouter from 'vue-router';


// define route
Vue.use(VueRouter);
export const router = new VueRouter({
    routes: [

    ],
    scrollBehavior (to, from, savedPosition) {
        return { x: 0, y: 0 }
    }
});