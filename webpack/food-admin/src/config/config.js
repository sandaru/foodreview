var SiteConfig = {
    app_name: "SethPhat Food Review Maintenance",

    // api config
    api_url: "http://b2be.sethphat.com/api/v1", // no slash at the end

    // resource url (image, attachment,...)
    resource_url: "http://b2be.sethphat.com",


};


export {
    SiteConfig
}