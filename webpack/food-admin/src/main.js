import Vue from 'vue'
import {router} from './config/router';
import App from './App.vue'
import store from './store';

// import dependency
import 'jquery';
import 'popper.js';
import 'bootstrap';
import * as _ from 'underscore';

// set globally
window.axios = axios;
window._ = _;
window.$ = window.jQuery = require('jquery');

// import style/css


// import js (es5)

new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App)
})
