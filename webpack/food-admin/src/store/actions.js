import * as types from './mutation-types'

// category
export const setCategory = ({commit}, data) => {
    commit(types.SET_CATEGORY_KEY, data);
};
