@extends('email.template')

@section('email_content')
    <table style="min-width:320px;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#eaeced">
        <!-- fix for gmail -->
        <tr>
            <td class="hide">
                <table width="600" cellpadding="0" cellspacing="0" style="width:600px !important;">
                    <tr>
                        <td style="min-width:600px; font-size:0; line-height:0;">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="wrapper" style="padding:0 10px;">
                <!-- module 1 -->
                <table data-module="module-1" width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td data-bgcolor="bg-module" bgcolor="#eaeced">
                            <table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="padding:29px 0 30px;">
                                        <table width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <th class="flex" width="113" align="left" style="padding:0;">
                                                    <table class="center" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td style="line-height:0; font-size: 18px;">
                                                                <a target="_blank" style="text-decoration:none;" href="{{url('/')}}">
                                                                    {{config('sethfood.SITE_NAME')}}
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </th>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <!-- module 2 -->
                <table data-module="module-2" data-thumb="thumbnails/02.png" width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td data-bgcolor="bg-module" bgcolor="#eaeced">
                            <table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td data-bgcolor="bg-block" class="holder" style="padding:58px 60px 52px;" bgcolor="#f9f9f9">
                                        <table width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td data-color="title" data-size="size title" data-min="25" data-max="45" data-link-color="link title color" data-link-style="text-decoration:none; color:#292c34;" class="title" align="center" style="font:35px/38px Arial, Helvetica, sans-serif; color:#292c34; padding:0 0 24px;">
                                                    Thông báo bình chọn địa điểm
                                                </td>
                                            </tr>
                                            <tr>
                                                <td data-color="text" data-size="size text" data-min="10" data-max="26" data-link-color="link text color" data-link-style="text-decoration:underline; color:#40aceb;" align="left" style="font: 16px/25px Arial, Helvetica, sans-serif; color:#888; padding:0 0 23px;">
                                                    <p>Xin chào, {{$user_name}}.</p>
                                                    <p>Thành viên <b>{{$voteInfo->CreatedByName}}</b> trong nhóm <b>{{$groupInfo->Name}}</b> của bạn vừa tạo ra một bình chọn địa điểm mới.</p>
                                                    <p>Bạn có thể nhấn vào nút ở bên dưới đây để bắt đầu bình chọn!</p>
                                                    <p>Bình chọn sẽ kết thúc vào lúc: <b>{{$voteInfo->EndTime}}</b> ngày hôm nay ({{\Illuminate\Support\Carbon::parse($voteInfo->CreatedDate)->format("d/m/Y")}}).</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding:0 0 20px;">
                                                    <table width="134" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td data-bgcolor="bg-button" data-size="size button" data-min="10" data-max="16" class="btn" align="center" style="font:12px/14px Arial, Helvetica, sans-serif; color:#f8f9fb; text-transform:uppercase; mso-padding-alt:12px 10px 10px; border-radius:2px;" bgcolor="#7bb84f">
                                                                <a target="_blank" style="text-decoration:none; color:#f8f9fb; display:block; padding:12px 10px 10px;" href="{{url('/#/VoteInfo/'. encode_id($voteInfo->ID))}}">
                                                                    Bình chọn ngay
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td data-color="text" data-size="size text" data-min="10" data-max="26" data-link-color="link text color" data-link-style="text-decoration:underline; color:#40aceb;" align="left" style="font: 16px/25px Arial, Helvetica, sans-serif; color:#888; padding:0 0 23px;">
                                                    <p>Xin cám ơn.</p>
                                                    <p>Hệ thống Email của {{config('sethfood.SITE_NAME')}}</p>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr><td height="28"></td></tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
@endsection