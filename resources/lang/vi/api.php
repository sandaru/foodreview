<?php

return [
    'post_not_existed' => 'Địa điểm không tồn tại.',
    'category_not_existed' => 'Chuyên mục không tồn tại.',

    'add_cmt_success' => 'Thêm đánh giá cho địa điểm thành công. Chúng tôi sẽ kiểm duyệt đánh giá của bạn trước khi đưa lên. Xin cám ơn.',
    'cmt_today_post' => 'Bạn đã có comment trong địa điểm này ngày hôm nay rồi!',
    'max_cmt_today' => 'Bạn đã có :today bình luận trong hôm nay rồi, xin hãy bình luận tiếp vào ngày mai. Xin cám ơn.',

    'create_post_failed' => 'Tạo địa điểm mới thất bại, xin hãy thử lại sau.',
    'create_post_ok_verify' => 'Tạo địa điểm mới thành công, chúng tôi sẽ kiểm duyệt bài viết của bạn trước khi đăng lên. Xin cám ơn sự hợp tác của bạn.',

    'upload_image_failed' => 'Tải lên hình ảnh thất bại, xin hãy thử lại.',
    'updated_profile' => 'Đã chỉnh sửa profile thành công.',
    'updated_password' => 'Đã chỉnh sửa mật khẩu thành công.',
    'old_pass_wrong'    => 'Mật khẩu cũ không chính xác.',

    'user_not_found' => 'User này không tồn tại.',

    // auth mess
    'wrong_password' => 'Mật khẩu không chính xác.',
    'login_success' => 'Đăng nhập thành công',
    'register_success' => 'Đăng ký thành công, bạn có thể bắt đầu đăng nhập.',
    'forgot_success' => 'Bạn đã yêu cầu quên mật khẩu thành công. Xin hãy làm theo sự chỉ dẫn của trong Email mà chúng tôi vừa gửi. Xin cám ơn!',

    // pers
    'permission_denied' => 'Không có quyền thao tác này.',

    // group mess
    'created_group' => 'Tạo nhóm mới thành công!',
    'group_not_existed' => 'Không tìm thấy nhóm này.',
    'not_in_group' => 'Bạn không thuộc nhóm này, không thể thực hiện hành động này!',

    // vote mess
    'vote_not_existed' => 'Bình chọn này không có thực',
    'created_vote' => 'Tạo bình chọn mới thành công!',
    'edited_vote' => 'Sửa bình chọn thành công!',
    'deleted_vote' => 'Đã xóa bình chọn thành công!',
    'vote_item_not_existed' => 'Mục bình chọn này không có thực',
    'vote_out_of_time' => 'Thời gian bình chọn đã hết, bạn không thể bình chọn nữa!',
    'vote_out_of_time_edit' => 'Thời gian bình chọn đã hết, bạn không thể sửa nữa!',

    // prepare time
    'prepare_time_1' => 'Dưới 5 phút',
    'prepare_time_2' => 'Từ 5 - 10 phút',
    'prepare_time_3' => 'Từ 10 - 20 phút',
    'prepare_time_4' => 'Từ 20 - 30 phút',
    'prepare_time_5' => 'Từ 30 phút trở lên',
];