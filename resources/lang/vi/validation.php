<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'The :attribute must be accepted.',
    'active_url'           => 'The :attribute is not a valid URL.',
    'after'                => 'The :attribute must be a date after :date.',
    'after_or_equal'       => 'The :attribute must be a date after or equal to :date.',
    'alpha'                => 'The :attribute may only contain letters.',
    'alpha_dash'           => 'The :attribute may only contain letters, numbers, dashes and underscores.',
    'alpha_num'            => 'The :attribute may only contain letters and numbers.',
    'array'                => 'The :attribute must be an array.',
    'before'               => 'The :attribute must be a date before :date.',
    'before_or_equal'      => 'The :attribute must be a date before or equal to :date.',
    'between'              => [
        'numeric' => 'The :attribute must be between :min and :max.',
        'file'    => 'The :attribute must be between :min and :max kilobytes.',
        'string'  => 'The :attribute must be between :min and :max characters.',
        'array'   => 'The :attribute must have between :min and :max items.',
    ],
    'boolean'              => 'The :attribute field must be true or false.',
    'confirmed'            => 'The :attribute confirmation does not match.',
    'date'                 => 'The :attribute is not a valid date.',
    'date_format'          => 'The :attribute does not match the format :format.',
    'different'            => 'The :attribute and :other must be different.',
    'digits'               => 'The :attribute must be :digits digits.',
    'digits_between'       => 'The :attribute must be between :min and :max digits.',
    'dimensions'           => 'The :attribute has invalid image dimensions.',
    'distinct'             => 'The :attribute field has a duplicate value.',
    'email'                => 'The :attribute must be a valid email address.',
    'exists'               => 'The selected :attribute is invalid.',
    'file'                 => 'The :attribute must be a file.',
    'filled'               => 'The :attribute field must have a value.',
    'gt'                   => [
        'numeric' => 'The :attribute must be greater than :value.',
        'file'    => 'The :attribute must be greater than :value kilobytes.',
        'string'  => 'The :attribute must be greater than :value characters.',
        'array'   => 'The :attribute must have more than :value items.',
    ],
    'gte'                  => [
        'numeric' => 'The :attribute must be greater than or equal :value.',
        'file'    => 'The :attribute must be greater than or equal :value kilobytes.',
        'string'  => 'The :attribute must be greater than or equal :value characters.',
        'array'   => 'The :attribute must have :value items or more.',
    ],
    'image'                => 'The :attribute must be an image.',
    'in'                   => 'The selected :attribute is invalid.',
    'in_array'             => 'The :attribute field does not exist in :other.',
    'integer'              => 'The :attribute must be an integer.',
    'ip'                   => 'The :attribute must be a valid IP address.',
    'ipv4'                 => 'The :attribute must be a valid IPv4 address.',
    'ipv6'                 => 'The :attribute must be a valid IPv6 address.',
    'json'                 => 'The :attribute must be a valid JSON string.',
    'lt'                   => [
        'numeric' => 'The :attribute must be less than :value.',
        'file'    => 'The :attribute must be less than :value kilobytes.',
        'string'  => 'The :attribute must be less than :value characters.',
        'array'   => 'The :attribute must have less than :value items.',
    ],
    'lte'                  => [
        'numeric' => 'The :attribute must be less than or equal :value.',
        'file'    => 'The :attribute must be less than or equal :value kilobytes.',
        'string'  => 'The :attribute must be less than or equal :value characters.',
        'array'   => 'The :attribute must not have more than :value items.',
    ],
    'max'                  => [
        'numeric' => 'The :attribute may not be greater than :max.',
        'file'    => 'The :attribute may not be greater than :max kilobytes.',
        'string'  => 'The :attribute may not be greater than :max characters.',
        'array'   => 'The :attribute may not have more than :max items.',
    ],
    'mimes'                => 'The :attribute must be a file of type: :values.',
    'mimetypes'            => 'The :attribute must be a file of type: :values.',
    'min'                  => [
        'numeric' => 'The :attribute must be at least :min.',
        'file'    => 'The :attribute must be at least :min kilobytes.',
        'string'  => 'The :attribute must be at least :min characters.',
        'array'   => 'The :attribute must have at least :min items.',
    ],
    'not_in'               => 'The selected :attribute is invalid.',
    'not_regex'            => 'The :attribute format is invalid.',
    'numeric'              => 'The :attribute must be a number.',
    'present'              => 'The :attribute field must be present.',
    'regex'                => 'The :attribute format is invalid.',
    'required'             => 'The :attribute field is required.',
    'required_if'          => 'The :attribute field is required when :other is :value.',
    'required_unless'      => 'The :attribute field is required unless :other is in :values.',
    'required_with'        => 'The :attribute field is required when :values is present.',
    'required_with_all'    => 'The :attribute field is required when :values is present.',
    'required_without'     => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same'                 => 'The :attribute and :other must match.',
    'size'                 => [
        'numeric' => 'The :attribute must be :size.',
        'file'    => 'The :attribute must be :size kilobytes.',
        'string'  => 'The :attribute must be :size characters.',
        'array'   => 'The :attribute must contain :size items.',
    ],
    'string'               => 'The :attribute must be a string.',
    'timezone'             => 'The :attribute must be a valid zone.',
    'unique'               => 'The :attribute has already been taken.',
    'uploaded'             => 'The :attribute failed to upload.',
    'url'                  => 'The :attribute format is invalid.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],

        // create a new post or comment
        'Title' => [
            'required' => 'Bạn phải nhập tiêu đề',
            'min'       => 'Tiêu đề phải từ :min ký tự trở lên',
            'max'       => 'Tiêu đề phải từ :max ký tự trở xuống',
        ],
        'CategoryID' => [
            'required' => 'Bạn phải chọn một chuyên mục',
        ],
        'ShortDescription' => [
            'required' => 'Bạn phải nhập giới thiệu ngắn',
            'max'       => 'Giới thiệu ngắn phải từ :max ký tự trở xuống'
        ],
        'Description' => [
            'required' => 'Bạn phải nhập nội dung',
            'min'       => 'Nội dung phải từ :min ký tự trở lên',
            'max'       => 'Nội dung phải từ :max ký tự trở xuống',
        ],
        'Address' => [
            'required' => 'Bạn phải nhập địa chỉ',
            'max' => 'Địa chỉ tối đa :max ký tự trở xuống'
        ],
        'Website' => [
            'max' => 'Website tối đa :max ký tự trở xuống'
        ],
        'Phone' => [
            'max' => 'Website tối đa :max ký tự trở xuống'
        ],
        'PrepareTime' => [
            'required' => 'Bạn phải chọn thời gian chuẩn bị món'
        ],
        'MinPrice' => [
            'required' => 'Bạn phải nhập giá tiền tối thiểu',
            'min' => 'Giá tiền tối thiểu phải từ :min trở lên',
            'max' => 'Giá tiền tối thiểu phải từ :max trở xuống',
        ],
        'MaxPrice' => [
            'required' => 'Bạn phải nhập giá tiền tối đa',
            'min' => 'Giá tiền tối đa phải từ :min trở lên',
            'max' => 'Giá tiền tối đa phải từ :max trở xuống',
            'gt' => 'Giá tiền tối đa phải lớn hơn giá tiền tối thiểu.'
        ],
        'Attributes' => [
            'required' => 'Bạn phải chọn đặc điểm của quán.',
        ],
        'PostItems' => [
            'required' => 'Bạn phải nhập ít nhất một thực đơn của quán.',
        ],
        'PostItems.*.Title' => [
            'required' => 'Các tiêu đề món ăn phải được nhập.',
            'max' => 'Các tiêu đề món ăn tối đa :max ký tự.',
            'min' => 'Các tiêu đề món ăn tối thiểu :min ký tự.',
        ],
        'PostItems.*.Description' => [
            'max' => 'Các nội dung món ăn tối đa :max ký tự.',
        ],
        'PostItems.*.Price' => [
            'required' => 'Giá các món ăn phải được nhập.',
            'max' => 'Giá các món ăn tối đa :max VND.',
            'min' => 'Giá các món ăn tối thiểu :min VND.',
        ],
        'PostItems.*.Image' => [
            'max' => 'Dung lượng các hình ảnh món ăn trong thực đơn phải nhỏ hơn :max KB',
            'image' => 'Các hình ảnh món ăn trong thực đơn phải là hình ảnh',
        ],
        'Gallery.*' => [
            'image' => 'Các hình ảnh trong thông tin phải là hình ảnh.',
            'max' => 'Dung lượng các hình ảnh trong thông tin phải nhỏ hơn :max KB',
        ],

        // auth
        'Email' => [
            'required' => 'Bạn phải nhập địa chỉ Email.',
            'email' => 'Địa chỉ Email sai định dạng.',
            'exists' => 'Địa chỉ Email này không có trong hệ thống của chúng tôi.',
            'unique' => 'Địa chỉ Email này đã có trong hệ thống.'
        ],
        'Password' => [
            'required' => 'Bạn phải nhập mật khẩu',
            'min' => 'Mật khẩu phải từ :min kí tự trở lên'
        ],
        'FirstName' => [
            'required' => 'Bạn phải nhập Tên',
            'max' => 'Tên của bạn phải từ :max kí tự trở xuống',
        ],
        'LastName' => [
            'required' => 'Bạn phải nhập Họ',
            'max' => 'Họ của bạn phải từ :max kí tự trở xuống',
        ],

        // comment
        'Rating' => [
            'required' => 'Bạn phải chọn số sao đánh giá',
            'min' => 'Số sao phải từ :min trở lên',
            'max' => 'Số sao phải từ :max trở xuống',
        ],
        'Comment' => [
            'required' => 'Bạn phải nhập đánh giá của bạn.',
            'min' => 'Đánh giá của bạn phải từ :min kí tự trở lên',
            'max' => 'Đánh giá của bạn phải từ :max kí tự trở xuống',
        ],

        // Group
        'Name' => [
            'required' => 'Bạn phải nhập Tên',
            'max' => 'Tên phải từ :max kí tự trở xuống',
        ],
        'Members' => [
            'required' => 'Bạn phải thêm thành viên vào nhóm',
            'min'      => 'Bạn phải thêm ít nhất :min thành viên vào nhóm',
            'array'    => 'Sai cấu trúc thành viên, không thể tiếp tục'
        ],
        'GroupImage' => [
            'required' => 'Bạn phải chọn hình nhóm',
            'image'     => 'Định dạng hình nhóm không hỗ trợ, xin hãy thử lại',
            'max'       => 'Hình nhóm phải từ :max KB trở xuống'
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
