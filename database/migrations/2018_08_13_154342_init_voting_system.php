<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitVotingSystem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("Group", function (Blueprint $table) {
            $table->increments("ID");

            $table->string("Name");
            $table->string("Description")->nullable();
            $table->string("GroupImage")->nullable();
            $table->integer("AdminUserID")->nullable()->unsigned();
            $table->boolean("Activate")->default(true);

            $table->integer("CreatedByID");
            $table->string("CreatedByName");
            $table->dateTime("CreatedDate");
            $table->integer("ModifiedByID");
            $table->string("ModifiedByName");
            $table->dateTime("ModifiedDate");

            $table->foreign("AdminUserID")->references('ID')->on('User');
        });

        Schema::create("GroupMember", function (Blueprint $table) {
             $table->integer("GroupID")->unsigned()->index();
             $table->integer("UserID")->unsigned()->index();

            $table->foreign("UserID")->references('ID')->on('User');
            $table->foreign("GroupID")->references('ID')->on('Group');
        });

        Schema::create("VotePlace", function (Blueprint $table) {
            $table->increments("ID");
            $table->integer("CreatorUserID")->nullable()->unsigned();
            $table->integer("GroupID")->nullable()->unsigned();
            $table->string("Title");
            $table->time('EndTime');
            $table->boolean("Activate")->default(true);

            $table->integer("CreatedByID");
            $table->string("CreatedByName");
            $table->dateTime("CreatedDate");
            $table->integer("ModifiedByID");
            $table->string("ModifiedByName");
            $table->dateTime("ModifiedDate");

            $table->foreign("CreatorUserID")->references('ID')->on('User');
            $table->foreign("GroupID")->references('ID')->on('Group');
        });

        Schema::create("VotePlaceItem", function (Blueprint $table) {
            $table->increments("ID");
            $table->integer("PostID")->unsigned();
            $table->integer('VotePlaceID')->unsigned();

            $table->foreign("PostID")->references('ID')->on('Post');
            $table->foreign("VotePlaceID")->references('ID')->on('VotePlace');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists("VotePlaceItem");
        Schema::dropIfExists("VotePlace");
        Schema::dropIfExists("GroupMember");
        Schema::dropIfExists("Group");
    }
}
