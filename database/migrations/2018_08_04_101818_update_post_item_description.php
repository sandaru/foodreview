<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePostItemDescription extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Illuminate\Support\Facades\DB::statement("
            ALTER TABLE `PostItem` CHANGE `Description` `Description` TEXT NULL DEFAULT NULL;
        ");
        \Illuminate\Support\Facades\DB::statement("
            ALTER TABLE `PostItem` CHANGE `Price` `Price` DECIMAL(20,0) DEFAULT 0;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Illuminate\Support\Facades\DB::statement("
            ALTER TABLE `PostItem` CHANGE `Description` `Description` TEXT;
        ");
        \Illuminate\Support\Facades\DB::statement("
            ALTER TABLE `PostItem` CHANGE `Price` `Price` DECIMAL(8,2) DEFAULT 0;
        ");
    }
}
