<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('User', function (Blueprint $table) {
            $table->increments('ID');
            $table->string('Email')->unique();
            $table->string('Password');
            $table->string('FirstName');
            $table->string('LastName');
            $table->string('ProfileImage')->nullable();
            $table->string("ApiToken")->nullable();
            $table->enum('Role', ['admin', 'user'])->default('user');

            $table->integer("CreatedByID");
            $table->string("CreatedByName");
            $table->dateTime("CreatedDate");
            $table->integer("ModifiedByID");
            $table->string("ModifiedByName");
            $table->dateTime("ModifiedDate");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('User');
    }
}
