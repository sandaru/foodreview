<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PostItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("PostItem", function (Blueprint $table) {
            $table->increments('ID');
            $table->integer("PostID")->unsigned();

            $table->string("Title");
            $table->text("Description");
            $table->string("Image")->nullable();
            $table->decimal("Price")->default(0);

            $table->integer("CreatedByID");
            $table->string("CreatedByName");
            $table->dateTime("CreatedDate");
            $table->integer("ModifiedByID");
            $table->string("ModifiedByName");
            $table->dateTime("ModifiedDate");

            // foreign key
            $table->foreign("PostID")->references('ID')->on('Post');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists("PostItem");
    }
}
