<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePost extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create("Post", function (Blueprint $table) {
            $table->increments('ID');
            $table->integer("CategoryID")->unsigned();

            $table->string("Title");
            $table->text("Description");
            $table->string("Address");
            $table->text("Gallery")->nullable();
            $table->decimal("MinPrice", 20, 0)->default(0);
            $table->decimal("MaxPrice", 20, 0)->default(0);

            $table->integer("CreatedByID");
            $table->string("CreatedByName");
            $table->dateTime("CreatedDate");
            $table->integer("ModifiedByID");
            $table->string("ModifiedByName");
            $table->dateTime("ModifiedDate");

            // foreign key
            $table->foreign("CategoryID")->references('ID')->on('Category');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists("Post");
    }
}
