<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitAttributeAndUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create("Attribute", function (Blueprint $table) {
            $table->increments("ID");
            $table->string("Name");
            $table->string("Description");
        });

        Schema::create("PostAttribute", function (Blueprint $table) {
            $table->integer("PostID")->unsigned();
            $table->integer("AttributeID")->unsigned();

            $table->foreign('PostID')->references('ID')->on('Post');
            $table->foreign('AttributeID')->references('ID')->on('Attribute');
        });

        Schema::table("Post", function (Blueprint $table) {
            $table->string("ShortDescription")->nullable();
            $table->string("Phone")->nullable();
            $table->string("Website")->nullable();
            $table->string("PrepareTime")->nullable();
            $table->boolean('Activate')->default(true);
            $table->boolean("Verified")->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists("PostAttribute");
        Schema::dropIfExists("Attribute");

        Schema::table("Post", function (Blueprint $table) {
            $table->dropColumn("ShortDescription");
            $table->dropColumn("Phone");
            $table->dropColumn("Website");
            $table->dropColumn("PrepareTime");
            $table->dropColumn("Activate");
            $table->dropColumn("Verified");
        });
    }
}
