<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitVoteItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("VotePlaceItemEntry", function (Blueprint $table) {
            $table->integer("VotePlaceItemID")->unsigned()->index();
            $table->integer('UserID')->unsigned()->index();
            $table->dateTime("CreatedDate");

            $table->foreign("UserID")->references('ID')->on('User');
            $table->foreign("VotePlaceItemID")->references('ID')->on('VotePlaceItem');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("VotePlaceItemEntry");
    }
}
