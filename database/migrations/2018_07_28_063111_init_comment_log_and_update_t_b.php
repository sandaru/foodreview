<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitCommentLogAndUpdateTB extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("Comment", function (Blueprint $tb) {
            $tb->boolean("Verified")->default(false);
        });

        Schema::create("UserLogComment", function (Blueprint $table) {
             $table->bigIncrements("ID");
             $table->integer("UserID");
             $table->integer("CommentID")->nullable();
             $table->dateTime('CreatedDate');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("Comment", function (Blueprint $tb) {
            $tb->dropColumn("Verified");
        });

        Schema::dropIfExists("UserLogComment");
    }
}
