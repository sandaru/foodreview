<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create("Comment", function (Blueprint $table) {
            $table->increments('ID');
            $table->integer("PostID")->unsigned();
            $table->integer("UserID")->unsigned();

            $table->string("Title")->nullable();
            $table->text("Comment");
            $table->text("Gallery")->nullable();
            $table->integer("Rating")->default(0);

            $table->integer("CreatedByID");
            $table->string("CreatedByName");
            $table->dateTime("CreatedDate");
            $table->integer("ModifiedByID");
            $table->string("ModifiedByName");
            $table->dateTime("ModifiedDate");

            // foreign key
            $table->foreign("PostID")->references('ID')->on('Post');
            $table->foreign("UserID")->references('ID')->on('User');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists("Comment");
    }
}
